const productDataAccess = require('../data-access/productDataAccess')
exports.create = (product) => {
  if(product.quantity > 10 ){
    return 'NO se puede ingresar un producto con quantity mayor a 10!'
  }else{
    return productDataAccess.create(product)
  }
}

exports.findAll = () => {
  return productDataAccess.findAll()
}

exports.findOne = (id) => {
  return productDataAccess.findOne(id)
}

exports.update = (id,product) => {
  return productDataAccess.update(id,product)
}

exports.delete = (id) => {
  return productDataAccess.delete(id)
}
