const express = require('express')
const app = express()

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.listen(3001,function(){
  console.log("server start at port 3001")
});

require('./routes/routes.js')(app)
