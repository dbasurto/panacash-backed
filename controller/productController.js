const productService = require('../service/productService');
exports.create = (req,res) => {
  res.send(productService.create(req.body))
}

exports.findAll = (req,res) => {
  res.send(productService.findAll())
}

exports.findOne = (req,res) => {
  res.send(productService.findOne(req.params.id))
}

exports.update = (req,res) => {
  res.send(productService.update(req.params.id,req.body))
}

exports.delete = (req,res) => {
  res.send(productService.delete(req.params.id))
}
