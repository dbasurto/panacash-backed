module.exports = (app) => {
    const productController = require('../controller/productController.js')

    app.post('/product', productController.create)

    app.get('/product', productController.findAll)

    app.get('/product/:id', productController.findOne)

    app.put('/product/:id', productController.update)

    app.delete('/product/:id', productController.delete)
}
