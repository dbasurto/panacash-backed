
var now = new Date();


let products = [{id:1, name:'abc', description:'cba', quantity:1, warranty:'Si', price:0.0, status:'Active', createdAt:now}]
exports.create = (product) => {
  products.push(product)
  return product
}

exports.findAll = () => {
  return products
}

exports.findOne = (id) => {
  return products.map(product => {
    if (product.id == id) {
      return product
    }
    return {}
  })
}

exports.update = (id,product) => {
  products.forEach(function(pro) {
    if(product.id == id){
      pro.name = product.name
      pro.description = product.description
      pro.quantity = product.quantity
      pro.price = product.price
      pro.status = product.status
      pro.warranty = product.warranty
    }
  })
  return product
}

exports.delete = (id) => {
  products = products.filter(product => product.id != id)
  return products
}
